package com.thun.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        val name = intent.getStringExtra(AlarmClock.EXTRA_MESSAGE)

        val textView = findViewById<TextView>(R.id.name).apply {
            text = name
        }
    }
}
