package com.thun.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.thun.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    companion object{
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fullName: TextView = findViewById(R.id.fullName)
        val stuId: TextView = findViewById(R.id.stuId)
        val button: Button = findViewById(R.id.button)
        button.setOnClickListener{
            Log.d(TAG, ""+fullName.text)
            Log.d(TAG, ""+stuId.text)
            val message = fullName.text

            val intent = Intent(this, HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE,message)
            }
            startActivity(intent)
        }
    }
}
